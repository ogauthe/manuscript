\chapter{SU(3) AKLT state}
\label{chap:AKLT3}
The AKLT state~\cite{affleck_rigorous_1987}  has been a breakthrough in the comprehension of integer spin chains and the so-called Haldane phase. In this chapter, we will recall the key points of AKLT physics and extend it to 2D and to SU(3).  We show that it can be represented 
as a simple tensor network, allowing  extensive studies. We explore its bulk properties on an infinite cylinder using transfer matrix methods.
The edge physics is investigated by computing the entanglement spectrum and the related entanglement Hamiltonian. We show that the latter can be very well approximated 
by a simple SU(3) Heisenberg Hamiltonian with exponentially decaying interactions. This Hamiltonian acts on virtual variables that are quarks and anti-quarks, therefore the edge modes are fractional  and attest an SPT phase.
This chapter is adapted from the article~\cite{gauthe_entanglement_2017}. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{AKLT physics}
As we stated in section \ref{sec:magn}, the LSM theorem imposes the ground state of a 1D half-integer spin Hamiltonian to be critical or degenerate. In 1983, Haldane showed that unexpectedly integer spins behave differently~\cite{haldane_nonlinear_1983}.
While the bilinear, half-integer spin  chain is critical with massless spinons excitations, he showed that the bilinear spin-1 chain with periodic boundary conditions has a unique, gapped ground state. Since the Hamiltonian is SU(2) symmetric and the ground state is unique, it has to be a singlet, otherwise each state of the multiplet would be a ground state. The underlying phase is called the Haldane phase, characterized by a gapped, symmetry-preserving ground state and fractional, spin-$1/2$ edge modes.
This phase can only exist for integer spins since it derogates the LSM theorem. It is experimentally relevant for several systems described by an effective spin-$1$ chain.

To understand this phase and especially its edge modes, Affleck, Kennedy, Lieb and Tasaki proposed in 1987 a paradigmatic, exact state for the spin-1 chain that does belong to the Haldane phase. The principle is to decompose every spin-1 of the chain into two spins $1/2$,
entangle all pairs of spins on the bonds into singlets and project pair of spins on every site onto physical spin-$1$.  A pictorial representation of the 1D spin-1 AKLT state is shown in
figure \ref{fig:aklt} (a).
The advantage of this construction is that its parent Hamiltonian is easy to find and connected to the purely bilinear Haldane chain. Indeed one can rewrite the most general SU(2)-invariant nearest-neighbor Hamiltonian presented in section \ref{sec:SUNpoint} with convenient constants and obtain
\begin{equation}
\mathcal{H}_{1BB} = \frac{1}{2}\sum_i \, [ {\bf S}_i\cdot{\bf S}_{i+1}+\beta({\bf S}_i\cdot{\bf S}_{i+1})^2+2/3 ],
\label{eq:Hspin1}
\end{equation}
which is the   Haldane chain Hamiltonian for $\beta=0$. For $\beta=1/3$, the Hamiltonian can be expressed as a sum of non-commuting projectors on the spin-$2$ subspace of the nearest neighbor bonds, i.e.
\begin{equation}
\mathcal{H}_\text{SU(2)}^{1D} = \mathcal{H}_{1BB}(\beta=1/3) = \sum_i \mathcal{P}_{i,i+1}^{S=2},
\end{equation}
where we have used the decomposition in terms of projectors  previously detailed.
Since $\mathcal{H}_\text{SU(2)}^{1D}$ is a sum of projectors, it is positive. It also annihilates the AKLT state of figure \ref{fig:aklt} (a), since two sites consists in four spins $1/2$, and after entangling two of them in a singlet it is no more possible to make a spin $2$ out of them. Hence the AKLT state is a ground state of the Hamiltonian (\ref{eq:Hspin1}) for $\beta=1/3$. It has been shown that on an infinite chain or with periodic boundary conditions, this ground state is unique. While it is not the ground state of the Haldane chain, numerical studies with varying $\beta$ from $1/3$ to $0$ have shown that the two states are adiabatically connected and belong to the same phase. With open boundary conditions, the two edge spins-$1/2$ are totally free and the ground state has a degeneracy of four. These are fractional excitations
that also exist in the Haldane chain and are protected by a symmetry~\cite{pollmann_symmetry_2012}, defining a particular SPT class~\cite{chen_symmetry-protected_2012, gu_symmetry-protected_2014, senthil_symmetry-protected_2015}.

The AKLT construction can be straightforwardly extended to 2D lattices. On the square lattice (or any 2D lattice of coordination $z=4$), one attaches four virtual spin-$1/2$ on each site, and then projects them onto the most symmetric (i.e. spin-2) irrep, as shown in figure \ref{fig:aklt}(b). Again, the parent Hamiltonian takes the simple form of a sum of projectors over all nearest neighbor bonds $\langle i,j\rangle$, $\mathcal{H}_\text{SU(2)}^{2D}=\sum_{\langle i,j\rangle}\mathcal{P}_{i,j}^{S=4}$. In 2D, the family of AKLT states are protected by SU(2) spin-rotations and one-site translation symmetries~\cite{takayoshi_field_2016}, a direct consequence of the LSM-Hastings theorem.

\begin{figure}
	\centering
		\includegraphics[width=0.8\textwidth]{figures/AKLT_basics}
		\caption{\footnotesize{The SU(2) spin-1 and spin-2 AKLT spin liquids in 1D (a) and 2D (b). Virtual spin-1/2 (orange circles) are entangled into singlets (ellipses). Dashed circles represent projectors on the largest spin irrep.
			}}
		\label{fig:aklt}	
\end{figure}

The 1D or 2D SU(2) AKLT states have extremely simple representations in terms of MPS~\cite{pollmann_symmetry_2012} and PEPS~\cite{cirac_entanglement_2011}, respectively, which make the analysis of their bulk and edge properties accurately computable.
Indeed, it is easy to see that the 1D SU(2) AKLT state of figure \ref{fig:aklt}(a) is in fact a MPS defined from a set of three
$2\times 2$ matrices labeled by the physical spin-$1$ with virtual spin-$1/2$ variables (i.e. $d=3$ and $D=2$). 
This construction can easily be generalized in 2D by replacing the $d$ matrices by $d$ rank-$z$ 
tensors, where $z$ is the lattice coordination number. In our case, we consider the square lattice with $z=4$. We take the most symmetric arrangement of the four spins $1/2$ and the elementary tensor has the coefficients
\begin{align*}
A^2[\uparrow,\uparrow,\uparrow,\uparrow] &= 1  \\
A^1[\uparrow,\uparrow,\uparrow,\downarrow] &= 1/2  \\
A^0[\uparrow,\uparrow,\downarrow,\downarrow] &= 1/\sqrt{6} \\
A^{-1}[\uparrow,\downarrow,\downarrow,\downarrow] &= 1/2  \\
A^{-2}[\downarrow,\downarrow,\downarrow,\downarrow] &= 1
\end{align*}
where all permutations of the four virtual variable have the same coefficient and therefore the five subtensors with fixed physical variable are normalized.


Although AKLT parent Hamiltonians are fine-tuned, the AKLT states provide in fact simple paradigms for
the simplest (non-topological) gapped spin liquid phases, which can occupy a rather extended region
in the parameter space of realistic Hamiltonians.  Since localized SU($N$) spin systems can now be realized on optical 1D and 2D lattices,
SU($N$) AKLT states are expected to describe generic spin liquid phases in such systems and are therefore of high interest. 
In the case of a SPT phase, the edge modes of the AKLT wave function will also be generic of the whole phase, 
being protected by symmetry. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{SU(3) AKLT wavefunction}

We now extend the recipe for the construction of SU(2) AKLT states to SU(3), in a straightforward way: the principle is to replace spins by irreducible representations of SU(3).  First, in order to realize SU(3) singlets on all nearest neighbor bonds of the square lattice, four ``quarks'' in the fundamental $[1]=\textbf{3}$ irrep (``antiquarks'' in the anti-fundamental $[1,1]=\overline{\textbf{3}}$ irrep) are attached on each even (odd) site. This way, neighboring virtual spins on every nearest neighbor bond belong to $\textbf{3}$ and $ \overline{\textbf{3}}$ irreps and can then be projected onto SU(3)
$\bullet=\textbf{1}$ singlets. Then, in order to entangle this simple product of singlets,
one projects the group of four quarks on each even (odd) site onto the most symmetric $[4]=\textbf{15}$
($[4,4]= \overline{\textbf{15}}$) irrep corresponding to the actual physical degrees of freedom, as seen in figure \ref{fig:pannel}(a). 
Note that the assignment as fundamental or anti-fundamental is arbitrary, the same tensor being placed on every site. 
As for SU(2), a simple parent Hamiltonian can be build from bond projectors on the largest, most-symmetric $[8,4]$ (self-conjugate) irrep obtainable from the tensor-product $\textbf{15}\otimes \overline{\textbf{15}}$, 
\begin{equation}
\mathcal{H}_{SU(3)}^{2D}=\sum_{\langle i,j\rangle}\mathcal{P}_{i,j}^{[8,4]}\, ,
\end{equation}
where the sum runs over all nearest neighbor bonds.

We already mentioned  the tensor corresponding to this wavefunction in table \ref{tab:tens_sym}. It can be constructed by diagonalizing the appropriate matrix  and applying the two lowering operators $S^{1-}$ and $S^{2-}$ to the known highest weight $(4,0)$ to obtain the generalized Clebsch-Gordon coefficients as we explained in section \ref{sec:TNsym}. Here, thanks to the simple form of the tensor it is not even necessary and the tensor can be obtained by pure combinatorics.
 We want a fully symmetric combination of four indistinguishable particles that have three different states, or in other words four  bosons three times degenerate. The dimension $\textbf{15}$ corresponds to  the number of 4-combination with three repetitions. The possible configurations are:
\begin{itemize}
\item 4 quarks in the same color: $\binom{3}{1} = 3$ distinguishable color states corresponding to the selected color, each of them with only 1 state of the Hilbert space with the appropriate color so a coefficient 1.
\item 3 quarks in one color, 1 quark in another one: $\binom{3}{1} \times \binom{2}{1} = 6$ color states, $\binom{4}{1}$ occurrences in the Hilbert space each so the coefficient is $1/2$
\item 2 quarks in one color, 2 in another one:  $\binom{3}{2} = 3$ color states, $\binom{4}{2}$ occurrences, coefficient $1/\sqrt{6}$.
\item 2 quarks in one color, 1 quark in a second, 1 quark in the third: $\binom{3}{1} = 3$ color states, $\binom{4}{2} \times \binom{2}{1} = 12$ occurences, coefficient $1/2\sqrt{3}$.
\end{itemize}
We check this makes the $\textbf{15}$ required states and the 81 color configurations of the Hilbert space are involved. The same argument gives the same tensor on odd site with conjugate ``anti-colors''.

\begin{figure}
	\centering
		\includegraphics[width=1.\textwidth]{figures/AKLT_pannel}
		\caption{\footnotesize{(a,b) The AKLT SU(3) wave function is defined similarly to the SU(2) case: four virtual states in the fundamental (anti-fundamental) irrep of SU(3) of dimension $D=3$, are attached on even (odd) sites
and projected onto the fully symmetric $ \textbf{15}$ ($\overline{\textbf{15}}$) irrep. 
Virtual states of all neighboring sites are projected on SU(3) singlets
to form a tensor network. (c) By contracting two identical site tensors on their physical indices one gets a new tensor $\mathbb{E}$ of dimension $D^2=9$. 
		}}
		\label{fig:pannel}	
\end{figure}


We now focus on the tensor network algorithm. For simplicity, let us first start with a periodic ($L$-site) 1D chain with $d$ on-site physical degrees 
of freedom labeled by $\alpha$ (e.g. the components of the physical spin).
As we detailed in chapter \ref{chap:TN}, the amplitudes $c_{\alpha_1\alpha_2\cdots\alpha_L}$ of  a (translation-invariant) MPS of virtual dimension $D$ are given solely in terms of $d$ $D\times D$ matrices $A^{\alpha}$ as $c_{\alpha_1\alpha_2\cdots\alpha_L}=\Tr \{ A^{\alpha_1} A^{\alpha_2}\cdots A^{\alpha_L} \}$. 
Here in 2D, the amplitudes of the PEPS are obtained from the tensor network defined by attaching a tensor on each lattice site and by contracting the site tensors over the virtual indices~\cite{cirac_renormalization_2009,cirac_entanglement_2012,schuch_condensed_2013,orus_advances_2014}.
The $S=2$ AKLT state of figure \ref{fig:aklt}(b) can then be viewed as a simple PEPS with $D=2$ virtual degrees of freedom (corresponding to the attached virtual spin-1/2) and $d=5$ physical spin components~\cite{cirac_entanglement_2011}.
Similarly, the SU(3) AKLT state can be interpreted as a PEPS of virtual dimension $D=3$ 
(for the three colors of the quarks) and $d=15$ physical dimension, as depicted in figure \ref{fig:pannel} (b). The virtual space being one staggered irrep, there is no bond operator to insert as we discussed in section \ref{sec:TNsym}.

To compute the PEPS wave function norm $\langle\Psi |\Psi\rangle$ and expectation values $\langle\Psi |O|\Psi\rangle$ of local operators $O$, we  defines the two-layer tensor network as discussed in section \ref{sec:PEPS}, each layer representing the ket and bra 
wave functions. By contracting two identical tensors on their physical indices one gets a new tensor $\mathbb{E}$ of dimension $D^2=9$, as shown in figure \ref{fig:pannel} (c). This way, the physical index disappears and its large dimension ($15$) is irrelevant for computations. We form an infinite cylinder by imposing periodic boundary conditions in one direction with circumference $N_v$.
Each row of the cylinder can then be seen as a transfer matrix, propagating states from the left to the right. This matrix acts on boundary states expressed in terms of virtual variables of the tensor network as shown in figure \ref{fig:cylinder}. To construct the fixed point boundary state of size $(D^2)^{N_v}$, one uses iterated powers / Lanczos algorithm to converge to the leading eigenvector / leading eigenvalues of the transfer matrix. Note that since the latter is a symmetric matrix, the left and right boundary states are identical.

\begin{figure}
	\centering
		\includegraphics[width=0.8\textwidth]{figures/cylinder}
		\caption{\footnotesize{The fixed-point boundary state is defined as the leading eigenvector of the transfer matrix. The latter is defined by contracting the local $\mathbb{E}$ tensor along a circle, leaving the left and right legs open.
		}}
		\label{fig:cylinder}	
\end{figure}

\begin{figure}[ht]
	\centering
		\includegraphics[width=0.70\textwidth]{figures/gap_weights_AKLT}
		\caption{\footnotesize{(a) Bulk gap of an infinite AKLT SU($N$) cylinder vs circumference $N_v$. The extrapolated $N_v\rightarrow\infty$ values of $\xi = 1/\Delta$ are shown on the plot. (b) Coefficients of the effective entanglement Hamiltonian (decomposed in term of Heisenberg-like operators) for SU(2) and SU(3) AKLT wavefunctions vs site separation (semi-log plot). Straight lines are fits according to an exponential behavior $J(r)=J_0 \exp(-r/\lambda)$. Data for SU(2) are taken from reference~\cite{cirac_entanglement_2011}. }}
		\label{fig:gap-weights}
\end{figure}

The gap $\Delta$ in the bulk can easily be computed from the two largest eigenvalues of the transfer matrix, $\Delta = \ln{(E_1/E_2)}$, with $E_1 > E_2$, the correlation length $\xi$ being defined as the inverse of the gap. We have computed $\Delta$ for cylinders of perimeter $N_v = 2, 4, 6, 8$ and extrapolated the result in the limit $N_v \rightarrow \infty$, as shown in figure \ref{fig:gap-weights} (a). We find that the extrapolation of $\xi$ for the SU(3) case is very short ($\xi_3 \simeq 1.2$), even shorter than the SU(2) value ($\xi_2 \simeq 2.1$). Note that the extrapolation is very accurate, the scaling being exponential and the system size being large compared to $\xi$.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Entanglement properties}
In order to construct the entanglement Hamiltonian, the fixed-point state is reshaped as a $D^{N_v} \times D^{N_v}$ boundary density matrix $\Sigma_b$, acting on virtual variables. As we discussed in section \ref{sec:PEPS}, it has previously been shown~\cite{cirac_entanglement_2011} that this matrix can be mapped onto the reduced density matrix of the half cylinder $\rho$ via an isometry $U$. Here, thanks to time-reversal and reflection symmetry, the left and right boundary states are real and identical and we can just rewrite Eq. (\ref{eq:holography}) as
$\rho = U\dg (\Sigma_b)^2 U$. The entanglement Hamiltonian $\mathcal{H}$ acting on virtual boundary configurations is defined via
$(\Sigma_b)^2 = \exp(-\mathcal{H})$, with an irrelevant temperature of 1.
 
Since $U$ is an isometry, the spectrum of $\mathcal{H}$ -- the entanglement spectrum -- is exactly the logarithm of the spectrum of the reduced density matrix $\rho$ up to a factor 2. Such a spectrum has been conjectured by Li and Haldane~\cite{li_entanglement_2008} to be in one-to-one correspondence with the physical edge modes of the system.
We compare the entanglement spectra of SU(2) and SU(3) AKLT wavefunctions in figure \ref{fig:specAKLT} (a) and (b). We observe they are very much similar at low energy: (i) the ground state is a singlet with momentum $k=0$ (when $N_v = 4n$), (ii) low-energy excitations follow a sinusoidal dispersion typical of the lower edge of a 2-spinon continuum, shown in figure \ref{fig:specAKLT} (c). This can be explained from the simple (approximate) analytical form of the entanglement Hamiltonian derived next.

%
\begin{figure}
	\centering
		\includegraphics[width=1.\textwidth]{figures/spectra_AKLT}
		\caption{Entanglement spectra on infinite cylinders of finite circumference $N_v$. (a) SU(2) AKLT wavefunction computed with $N_v=12$, irreps are indexed by their spin.  
		(b) SU(3) AKLT wavefunction computed with $N_v=8$, irreps are indexed according to their Young tableaux.
		(c) Comparison of the low-energy part of the two spectra superposed on the same graph (only trivial and adjoint irrep are kept, with new symbols for the SU(2) spectrum). The SU(2) spectrum is rescaled to match the first singlet excitation 
		(at $k=\pi$) of the two spectra. Lines are sinusoidal fits of the edge of the 2-spinon continuum.}
		\label{fig:specAKLT}	
\end{figure}
%



To understand its nature we decompose the entanglement Hamiltonian on the canonical basis of SU(3) operators acting on the virtual degrees of freedom at the boundary. The latter are being defined in a fermionic representation as 
\begin{equation}
S^\alpha_\beta (i) =  \left\{
  \begin{array}{l l}
    c\dg_{\alpha,i} c_{\beta,i} - \delta_{\alpha,\beta}/3  \quad \text{if $i$ is even}\\
   -c_{\alpha,i} c\dg_{\beta,i} + \delta_{\alpha,\beta}/3  \quad \text{if $i$ is odd}
  \end{array} \right.
\end{equation}
where $\alpha, \beta$ label the three SU(3) colors. Note that the definition takes into account the anti-fundamental representation on odd sites~\cite{affleck_large-n_1985}, which in the fermion language is obtained via a particle-hole transformation\footnote{Another possibility would be to use the 8 Gell-Mann matrices and their conjugate defined in \ref{eq:gellmann}, yielding the same $\textbf{S}\cdot \textbf{S}$ operator.}
Since the Hamiltonian is SU(3) invariant, there is a limited number of combination of operators that can appear, in particular no linear term can appear. The only second order SU(3) invariant terms are Heisenberg-like terms, $\textbf{S}_i \cdot \textbf{S}_j = \sum_{\alpha,\beta} S^\alpha_\beta (i) S^\alpha_\beta (j)$. Hence,
\begin{equation}
\mathcal{H} = E_0 + \sum_{i\neq j} J(\abs{i-j}) \,\textbf{S}_ i\cdot \textbf{S}_j + \mathcal{H}_\text{rest},
\label{eq:H_Heisenberg}
\end{equation}
where $E_0 = \Tr(\mathcal{H})$. The higher order terms $\mathcal{H}_\text{rest}$ are corrections of much lower weights --  only $5\%$ (6\%) of the euclidean norm of  $\mathcal{H} - E_0 $ for $N_v = 8$ ($N_v = 6$) -- and are expected to be irrelevant.
We show in figure \ref{fig:gap-weights}(b) that the weights $J(r)$ follow an exponential decay with distance, from with we can extract a typical decay length $\lambda$. The sign of those weights is staggered, meaning the interaction is antiferromagnetic between quarks and anti-quarks and ferromagnetic between two quarks. By comparing SU(3) and SU(2), we see that $\lambda_3<\lambda_2$, fulfilling the same inequality than the bulk correlation length $\xi_3<\xi_2$. This is in agreement with a general argument based 
on PEPS that the
range $\lambda$ of the entanglement Hamiltonian tracks the bulk correlation length $\xi$~\cite{cirac_entanglement_2011}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Interestingly, the entanglement Hamiltonian of the SU(3) AKLT state is adiabatically connected to the nearest neighbor $\textbf{3}-\overline{\textbf{3}}$ Heisenberg chain~\cite{affleck_large-n_1985}. The latter can be mapped to a spin-$1$ chain with a purely negative biquadratic coupling and was shown to
exhibit a small spontaneous 
dimerization~\cite{affleck_critical_1987,affleck_comment_1989,barber_spectrum_1989,sorensen_correlation_1990}. It is however plausible that the extra  $J(2)\sim 0.3\, J(1)$ coupling will close the gap and lead to a gapless spectrum. Indeed, the numerical entanglement spectra shown in figures~\ref{fig:specAKLT} (b,c) do not show any hint of spontaneous translation symmetry breaking (implying ground state two-fold degeneracy in the $N_v\rightarrow\infty$ limit), but such a feature is hard to see at high temperature.
The conformal field theory (CFT) description of our entanglement Hamiltonian is an open problem which would require the numerical treatment of very long chains.
Interestingly, the parent 
Hamiltonian~\cite{tu_quantum_2014,bondesan_infinite_2014} for a
 CFT wave function constructed from the $\mathrm{SU}(3)_1$ WZW models is, once truncated, quite similar to our quasi-local entanglement Hamiltonian, although with a larger ratio  $J(2)/J(1)\simeq 0.56$ and a three-body term of significant amplitude. Hence a description of the entanglement Hamiltonian in terms of a $\mathrm{SU}(3)_1$ WZW theory seems natural and, at least, agrees with our low-energy entanglement spectrum shown in figure \ref{fig:specAKLT} (c).
Tu et al.~\cite{tu_quantum_2014} report critical properties deviating from the expected behaviors of the $\mathrm{SU}(3)_1$ WZW model. We note however that the two
(local) models may sit in different critical phases. 

Another interesting question is the possible correspondence between the entanglement spectrum and the edge physics~\cite{li_entanglement_2008}.
As for the SU(2) case, one can construct a local SU(3)-invariant parent Hamiltonian or ``PEPS model''~\cite{perez-garcia_peps_2008,yang_edge_2014} for which any region with an open 1D boundary $\partial R$ will have a degenerate manifold of (at most) $D^{|{\partial R}|}$ ground state. As for any PEPS models in a trivial (i.e. short-ranged entangled) phase, any Hamiltonian can be realized on the edge~\cite{yang_edge_2014} by slightly perturbing the
(fine-tuned) SU(3) PEPS model. 
However, it is still possible to protect edge properties by
symmetries in the bulk~\cite{chen_symmetry-protected_2012}. For example, similarly to the SU(2) AKLT model,
SU(3) symmetry and translation invariance rule out a gapped edge which does not break any symmetry~\cite{lieb_two_1961}. This is in direct correspondence with the properties of the (infinite size) entanglement spectrum discussed above.

Lastly, we comment on the relevance of this work to cold atom physics. We detailed in section \ref{sec:coldatoms} how to simulate SU($N$) systems in cold atomic gases. The difficulty here is that this the irreps are staggered conjugate, which is
realizable experimentally although very challenging~\cite{laflamme_cpn-1_2016}. It requires SU(3) fermions with a staggered optical potential such that the occupation numbers are exactly 1 and 2 on the two sublattices.
To enforce the same irrep on every site, a different AKLT construction is needed, involving virtual states belonging to the 
smallest self-conjugate irrep. For SU(3) it corresponds to the adjoint $[2,1] = \textbf{8}$ irrep.
