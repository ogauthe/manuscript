%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\addchap{Introduction}

This thesis presents the results of three years of work under the direction of Sylvain Capponi and Didier Poilblanc inside the Laboratoire de Physique Théorique of the Université Paul Sabatier, Toulouse. It is mostly based on the results published in two articles  \cite{gauthe_entanglement_2017} and \cite{gauthe_su4_2019}, with a few additions. The source code used to run the simulations can be found at the url \url{https://framagit.org/ogauthe/PEPS-python}.

In this work, we aim to construct and determine the physical properties of paradigmatic wavefunctions of SU($N$) systems. The new possibilities offered by recent advances in cold atoms motivated us to explore the physics of solid states systems made of SU($N$)-symmetric components for $N > 2$. To this extend, we use the numerical methods of  tensor networks, which underwent a rapid expansion in the last decade.
We follow here an uncommon approach in the spirit of the Bardeen-Cooper-Schrieffer and Affleck-Kenney-Lieb-Tasaki states, focusing not on a given Hamiltonian but on wavefunctions. We construct fully symmetric wavefunctions of SU($N$) systems on a lattice, explore their properties and only afterwards 
 search for an associated Hamiltonian.
The  exciting features of topological physics inspired us first to construct an SU(3) symmetry protected topological phase phase and second  to explore a family of SU(4) topologically ordered states.


\section*{From angular momentum to SU(\texorpdfstring{$N$}{}): a history}


Symmetries play a key role in our current description of the natural word. In classical physics, symmetries are usually an elegant way to ease computations by foreguessing the direction or the dependencies of a given quantity. In quantum mechanics, symmetries have a much deeper role in the description of matter. Non-trivial results, such as the existence of spin, its link to statistics or even the form of the electron-photon interaction can be derived as pure consequences of those symmetries. Before diving into the core of our work, we propose here a brief summary of the discovery of SU($N$) representation theory role in theoretical physics.

This story begins with the quantization of angular momentum and spin.
In classical mechanics, the angular momentum is a three-dimensional vector that describes the rotation of a mass around a given point. The first developments of quantum physics in the context of atomic physics followed a classical approach and naturally considered this quantity. In 1913, Bohr introduced the quantization of the orbital angular momentum to describe the movement of the electron around the nucleus in the hydrogen atom. More precisely, he imposed the angular momentum to be a (non-zero) integer multiple of the reduced Planck constant. This postulate turned out to be very successful and allowed him to retrieve the spectrum of the hydrogen atom, but it had no physical ground or interpretation.

This hypothesis was then extended by Sommerfeld in what is called the \emph{old quantum theory}. The quantization of the angular momentum along an axis, called \emph{space quantization}, played a major role in this theory. However, it was seen as a mathematical abstraction with little physical meaning, not to be taken literally until Stern and Gerlach decided to experimentally probe it \cite{friedrich_stern_2003}. In 1922, they observe the splitting of a beam of silver atoms through an inhomogeneous magnetic field and understood it as a proof of the existence of space quantization: for an unknown reason, angular momentum had to be quantized in the real world, this was not just a theoretical trick.
The orbital momentum of the silver atom is actually zero: the result of Stern and Gerlach is a manifestation of spin, but the concept was not born yet and their interpretation was wrong.  Only in 1927 was the Stern and Gerlach experiment correctly interpreted, not until  spin had been thought up from other considerations.  Another of its manifestation, the anomalous Zeeman effect, was known since 1896 and kept defying theorists. 

The concept of spin was first introduced in 1924 by Wolfgang Pauli while he worked on electronic shell \cite{pauli_nobel_1946}. To explain the doublet structure of the alkali spectra, he introduced a new electronic degree of freedom that had to be two-valued and had no classical description. This led him to the formulation of his exclusion principle the following year. In 1926, Uhlenbeck and Goudsmit interpreted this degree of freedom as an intrinsic angular momentum due to the self-rotation of the electron and explained the anomalous Zeeman effect with it \cite{jackson_classical_1975}. 
Hence, at that time, the concept of spin as a quantized intrinsic angular momentum was accepted and the orbital angular momentum was experimentally proven to be quantized. Both of them were relevant and well-defined quantities that could be measured, but no theory explained their quantization.

In 1927, Pauli proposed the first quantum theory of spin in the new formalism of quantum mechanics. He used Pauli matrices to describe the three components of the spin operator and introduced the concept of spinor.
The following year, Dirac published his relativistic equation of the electron where spin naturally appears: the modern theoretical description of the spin was born \cite{basdevant_mecanique_2009}.
He  was also among the first to realize the importance of group in quantum physics.

From there, Weyl and Wigner  applied the mathematical framework of group theory to gain a  deeper understanding of the mathematical foundations  of quantum theory and the role of symmetries in this theory.
In 1931, Wigner proved the theorem that bears his name,  which stipulates that the bijective  transformations of a wavefunction have to be linear or antilinear unitary maps of the Hilbert space.
With these tools, spin and orbital angular momentum can be unified by the study of the  rotational group SO(3) and their quantization explained.  This group is closely tied to the complex unitary group SU(2), their  appearance in quantum physics is the consequence of the rotation invariance of the law of physics, which is a subpart of the larger Lorentz invariance.

In high energy physics, the concept of continuous symmetry group turned out to be extremely fruitful with the development of gauge theories. Gauge invariance was already known from classical electrodynamics, but quantum theory brought a new perspective on it. The first gauge theory, quantum electrodynamics, describes electromagnetism by the action of the gauge group U(1) on the wavefunction. The next step was taken by  Yang and  Mills in 1954, when they extended the concept of gauge theory to non-abelian groups. This fruitful work  allowed to treat general SU($N$) gauge group in quantum field theory.
In 1961,  Gell-Mann proposed his Eightfold Way based on SU(3) representation theory. He applied it to classify observed subatomic particles and accurately predict new ones. This was the first experimentally relevant use of SU($N$) for $N > 2$, which laid  the basis of quantum chromodynamics. 

Nowadays, the Standard Model describes the strong, weak and electromagnetic interactions through the gauge group $\mathrm{SU}(3) \times \mathrm{SU}(2)\times\mathrm{U}(1)$. Beyond Standard Model theories try to pursue unification of interactions with symmetries from larger SU($N$) groups or exceptional Lie groups, however these theories have so far failed to provide new insight experimentally relevant.

The successes of SU($N$) gauge theories in high-energy physics encouraged theorists to look for SU($N$) symmetry in other domains of physics, starting with solid state.
There, group theory already plays a major role with Landau theory  of phase transition, which is  based on spontaneous symmetry breaking. However, SU($N$) symmetry is absent from the fundamental description of condensed matter systems for $N > 2$. Indeed, the only relevant interaction is electromagnetism, with gauge group U(1), and general space-time invariance is ruled by the Lorentz group SO(1,3), associated with SU(2) only.

However, SU($N$) can \emph{emerge} as an effective model and be relevant to describe certain systems with additional symmetries. Furthermore, recent advances in the domain of cold atoms systems bring new possibilities to simulate systems with atoms trapped in optical lattices. With these techniques, experimental realization of systems exhibiting SU($N$) physics is no more the exclusivity of the domain of high energies
and can be achieved within a laboratory the size of a classroom.

\section*{Organization of this manuscript}
This manuscript consists in five chapters. The first three ones deal with  the general physical context, the mathematical theory and the algorithms. The two last ones present the results obtained using these elements.

The first chapter details how physicists turned to SU($N$) in the domain of condensed matter physics. It starts with an overview of quantum magnetism and the theoretical description of spins, which are ruled by the group SU(2), as well as the conditions for this symmetry to enhance to SU($N$). It then explains how the recent domain of cold atom physics allows  to \emph{design} new types of matter, including quantum simulators for SU($N$) systems. It concludes with an introduction of topological physics, which is an important motivation for our latter work.

In the second chapter we introduce the conceptual tools of representation theory, which is the mathematical framework needed to address SU($N$) physics. We start with general definitions and then focus on the representation of matrix Lie groups. We dive into the structure of the finite-dimensional representations of SU($N$) and describe the formalism of Young tableaux  used to label them. Finally, we get back to physics and review how this mathematical structure can be used to obtain major results on physical systems.

The third chapter is dedicated to tensor network methods. We summarize the core ideas of these algorithms and the key role of entanglement. We then detail the type of tensor network we use, the PEPS, and explain how to implement SU($N$) symmetry directly inside the elementary tensor, which dramatically reduces the number of degrees of freedom. We conclude with the description of the corner transfer matrix algorithm, which is the main algorithm we used in our work.


In the fourth chapter, we discuss the Affleck-Kenney-Lieb-Tasaki state, introduced in the binlinear-biquadratic spin-1 chain. We extend its construction to two dimensional lattices and replace SU(2) spins by representations of SU(3).
We explore the entanglement properties of this new state and prove it belongs to the class of symmetry protected topological states.

The fifth chapter covers resonant valence bond-like states. We generalize the concept to any representation of SU($N$) on a bipartite lattice and apply it to the staggered fundamental-conjugate representations on the square lattice. We then consider the special case of the two-fermion representation $\textbf{6}$ of SU(4), which is self-conjugate and allows a translation-invariant wavefunction. We construct a family of resonant valence bond-like states that can be either gapless or gapped spin liquids and exhibits its topological order.
Lastly, we search for a reasonable, local Hamiltonian that could host this phase.


We tried to base our dissertation on published sources whenever possible, however we acknowledge inspiration from a few unpublished ones. Those documents include A. Zheludev's \textit{Advanced Solid State Physics} course for the first chapter. For the second chapter, we refereed to  D. Bernard and D. Renard's lecture notes \textit{Éléments de théorie des groupes et symétries quantiques}, partially published in \cite{renard_groupes_2010}, to online course notes on group theory by J.-B. Zuber and to an online document on Young tableaux by G. Ferrera. The third chapter includes many hints  from course notes and slides by P. Corboz.


